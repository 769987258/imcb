using GalaSoft.MvvmLight;
using IMCBcm.Models;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace IMCBcm.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            //----------------------------------------------------------------------------------------------------------------------------//
            MenuModels = new ObservableCollection<MenuModel>();                                                                           //
            menuModels.Add(new MenuModel() { IconFont = "\xe610", Title = "导入合同", BackColor = "#87CEFA"});                            //  
            menuModels.Add(new MenuModel() { IconFont = "\xe608", Title = "水印生成", BackColor = "#87CEEB"});                            //
            menuModels.Add(new MenuModel() { IconFont = "\xe607", Title = "合同管理", BackColor = "#87CEFA"});                            //
            menuModels.Add(new MenuModel() { IconFont = "\xe60b", Title = "待办事项", BackColor = "#B0C4DE"});                            //
            menuModels.Add(new MenuModel() { IconFont = "\xe60d", Title = "财务审批", BackColor = "#6495ED"});                            //
                                                                                                                                          //
            MenuModel = MenuModels[0];                                                                                                    //
                                                                                                                                          //
            SelectedCommand = new RelayCommand<MenuModel>(t => Select(t));                                                                //
            SelectedTaskCommand = new RelayCommand<TaskInfo>(t => SelectedTask(t));                                                       //
            //----------------------------------------------------------------------------------------------------------------------------//

        }


        private ObservableCollection<MenuModel> menuModels;

        public ObservableCollection<MenuModel> MenuModels
        {
            get { return menuModels; }
            set { menuModels = value; RaisePropertyChanged(); }
        }

        private MenuModel menuModel;

        public MenuModel MenuModel
        {
            get { return menuModel; }
            set { menuModel = value;RaisePropertyChanged(); }
        }

        private TaskInfo info;

        public TaskInfo Info
        {
            get { return info; }
            set { info = value; RaisePropertyChanged(); }
        }

        public RelayCommand<MenuModel> SelectedCommand { get; set; }
        public RelayCommand<TaskInfo> SelectedTaskCommand { get; set; }

        private void Select(MenuModel model)
        {
            MenuModel = model;
        }

        public void AddTaskInfo(string content)
        {
            MenuModel.TaskInfos.Add(new TaskInfo()
            {
                Content = content
            });
        }

        public void SelectedTask(TaskInfo task)
        {
            Info = task;
            Messenger.Default.Send(task, "Expand");
        }
    }
}