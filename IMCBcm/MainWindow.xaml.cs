﻿using GalaSoft.MvvmLight.Messaging;
using IMCBcm.Models;
using IMCBcm.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinForm=System.Windows.Forms;


namespace IMCBcm
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            db = new DataBase();
            db.connect();
            db.command("CREATE TABLE Test (sno Integer, sname VarChar(20))");
            this.MouseDown += (sender, e) =>
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                    this.DragMove();
            };
            //Messenger.Default.Register<TaskInfo>(this, "Expand", ExpandColumn);
            this.DataContext = new MainViewModel();
        }

        //private void TextBox_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.Key == Key.Enter)
        //    {
        //        string inputValue = inputText.Text;
        //        if (inputValue == "") return;
        //
        //        var vm = this.DataContext as MainViewModel;
        //        vm.AddTaskInfo(inputValue);
        //        inputText.Text = string.Empty;
        //    }
        //}

       //private void ExpandColumn(TaskInfo task)
       //{
       //    var cdf = grc.ColumnDefinitions;
       //    if (cdf[1].Width == new GridLength(0))
       //    {
       //        cdf[1].Width = new GridLength(280);
       //        //btnmin.Foreground = new SolidColorBrush(Colors.Black);
       //        //btnmax.Foreground = new SolidColorBrush(Colors.Black);
       //        //btnclose.Foreground = new SolidColorBrush(Colors.Black);
       //    }
       //    else
       //    {
       //        cdf[1].Width = new GridLength(0);
       //        //btnmin.Foreground = new SolidColorBrush(Colors.White);
       //        //btnmax.Foreground = new SolidColorBrush(Colors.White);
       //        //btnclose.Foreground = new SolidColorBrush(Colors.White);
       //    }
       //
       //
       //}

        private void Border_Click(object sender, RoutedEventArgs e)//导入合同
        {
            //Window win = new Window();
            //win.Show();
        }

        private void Daorupdf_Click(object sender, RoutedEventArgs e)                     //若需要另一版文件浏览器（我的电脑样式）https://blog.csdn.net/ununie/article/details/80698920
        {
            WinForm.FolderBrowserDialog dialog = new WinForm.FolderBrowserDialog();
            dialog.ShowDialog();
        }









        

        private void RadioButton_Click1(object sender, RoutedEventArgs e)
        {
            DataContext = new ImportContract();
        }

        private void RadioButton_Click2(object sender, RoutedEventArgs e)
        {
            DataContext = new WaterMark();
        }

        private void RadioButton_Click3(object sender, RoutedEventArgs e)
        {
            DataContext = new ContractManagement();
        }

        private void RadioButton_Click4(object sender, RoutedEventArgs e)
        {
            DataContext = new ContractManagement();
        }

        private void RadioButton_Click5(object sender, RoutedEventArgs e)
        {
            DataContext = new ContractManagement();
        }
        //关于我们。。。。
        private void RadioButton_Click6(object sender, RoutedEventArgs e)
        {
            aboutus window = new aboutus();
            window.Show();

        }

        DataBase db;
    }
}
